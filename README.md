# Single file project example

This is how to call the most simple program in Haskell. You have to install
a Haskell compiler first, the most common is [GHC][GHC] which we will use in
this example.

Here are the three possibilities:

1. Compile and run the executable
```
ghc -o Main.exe Main.hs
./Main
```

2. Execute using `ghci` (interactive environment of GHC)
```ghci Main.hs```
This will print something like:
```
GHCi, version 8.10.2: https://www.haskell.org/ghc/  :? for help
[1 of 1] Compiling Main             ( Main.hs, interpreted )
Ok, one module loaded.
```
Then you just type in `main` (the name of the function) and the program will
get executed:

```
*Main> main
I'm a single haskell file that is not doing a lot, just printing this text!
*Main>
```
You can leave ghci again by typing `:quit` 

3. Via docker
```
docker build -t single-file-project .
docker run single-file-project
```
Note that if you run the file via docker, you don't need to install `GHC`
before (only [Docker or Docker for Desktop][docker-install]).  
Running with docker will perform the same steps as in 1.

[GHC]: https://www.haskell.org/ghc/download.html
[docker-install]: https://docs.docker.com/get-docker/

