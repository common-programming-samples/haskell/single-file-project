FROM haskell:latest
COPY Main.hs /
RUN ghc -o Main Main.hs
ENTRYPOINT ["/Main"]
